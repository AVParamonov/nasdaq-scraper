package hrytsenko.nasdaq.daemon;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import hrytsenko.nasdaq.domain.CompaniesService;
import hrytsenko.nasdaq.domain.data.Company;
import hrytsenko.nasdaq.endpoint.NasdaqEndpoint;
import hrytsenko.nasdaq.endpoint.data.NasdaqCompany;

public class NasdaqServiceTest {

    @Mock
    private CompaniesService companiesService;
    @Mock
    private NasdaqEndpoint nasdaqEndpoint;

    @InjectMocks
    private NasdaqService nasdaqService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        NasdaqCompany aapl = new NasdaqCompany("AAPL", "Apple Inc.", "Technology", "Computer Manufacturing");
        NasdaqCompany msft = new NasdaqCompany("MSFT", "Microsoft Corporation", "Technology",
                "Computer Software: Prepackaged Software");
        Mockito.doReturn(Arrays.asList(NasdaqCompany.toCompany(aapl), NasdaqCompany.toCompany(msft)))
                .when(nasdaqEndpoint).downloadCompanies("NASDAQ");

        NasdaqCompany orcl = new NasdaqCompany("ORCL", "Oracle Corporation", "Technology",
                "Computer Software: Prepackaged Software");
        Mockito.doReturn(Arrays.asList(NasdaqCompany.toCompany(orcl))).when(nasdaqEndpoint).downloadCompanies("NYSE");
    }

    @Test
    public void testUpdateCOmpanies() {
        nasdaqService.updateCompanies(Arrays.asList("NASDAQ", "NYSE"));

        Mockito.verify(companiesService, Mockito.times(3)).updateCompany(Matchers.any(Company.class));
        Mockito.verify(nasdaqEndpoint, Mockito.times(2)).downloadCompanies(Matchers.anyString());
        Mockito.verifyNoMoreInteractions(companiesService, nasdaqEndpoint);
    }

}
