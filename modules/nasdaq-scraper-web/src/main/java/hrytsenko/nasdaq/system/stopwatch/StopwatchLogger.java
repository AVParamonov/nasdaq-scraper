package hrytsenko.nasdaq.system.stopwatch;

import java.util.logging.Logger;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import com.google.common.base.Stopwatch;

@Interceptor
@WithStopwatch
public class StopwatchLogger {

    private static final Logger LOGGER = Logger.getLogger(StopwatchLogger.class.getName());

    @AroundInvoke
    public Object log(InvocationContext context) throws Exception {
        Stopwatch stopwatch = Stopwatch.createStarted();
        try {
            return context.proceed();
        } finally {
            LOGGER.info(() -> String.format("Invocation of %s::%s completed in %s.",
                    context.getTarget().getClass().getSimpleName(), context.getMethod().getName(), stopwatch));
        }
    }

}
